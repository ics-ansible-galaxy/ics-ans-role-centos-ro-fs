# ics-ans-role-centos-ro-fs

Ansible role to set the root filesystem readonly in centos7.

## Requirements

- ansible >= 2.7
- molecule >= 2.7

## Role Variables

```yaml
centos_ro_fs_auto_reboot: false  # setting this to true will reboot the machine at the end
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-centos-ro-fs
```

## License

BSD 2-clause
